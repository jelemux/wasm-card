#[derive(Clone)]
pub struct Download {
    pub file_name: String,
    pub content: String,
    pub mime_type: MimeType,
}

impl Download {
    pub fn as_data_link(&self) -> String {
        let data = base64::encode(&*self.content);
        let uri_component: String = js_sys::encode_uri_component(&data).into();

        format!("data:{};base64,{}", self.mime_type.as_text(), uri_component)
    }
}

#[derive(Clone, Copy)]
pub enum MimeType {
    PDF,
    VCard,
    SVG,
}

impl MimeType {
    pub fn as_text(&self) -> &str {
        match self {
            MimeType::PDF => "application/pdf",
            MimeType::VCard => "text/vcard",
            MimeType::SVG => "image/svg+xml",
        }
    }
}

#[derive(Clone, Copy, PartialEq)]
pub enum DownloadOption {
    PDF,
    VCard,
    QrCode,
}

#[derive(Clone, Debug, PartialEq)]
pub struct File {
    pub name: String,
    pub content: String,
}
