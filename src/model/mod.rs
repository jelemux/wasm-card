use crate::model::utility::File;
use crate::view::property_group::*;
use input_fields::VCardPropertyInputField;
use yew::prelude::*;

pub mod input_fields;
pub mod property_groups;
pub mod utility;
pub mod vcard;

/// Trait for types that represent the data of a vcard property used inside of a `VCardPropertyInputComponent`.
pub trait VCardPropertyInputGroupObject<M: 'static + PartialEq + Clone>: Clone + PartialEq
where
    Self: Sized,
{
    /// Function for creating a new (and empty) `VCardPropertyInputGroupObject`.
    fn new() -> Self;
    /// Getter function for the title of the component
    fn get_title(&self) -> String;
    /// Converts each field of the `VCardPropertyInputGroupObject` to a VCardPropertyInputField and returns them as a vector.
    fn get_input_fields(
        &self,
        link: &ComponentLink<PropertyGroupInputComponent<Self, M>>,
    ) -> Vec<VCardPropertyInputField>;
    fn update(
        &mut self,
        props: InputProps<Self, M>,
        msg: <PropertyGroupInputComponent<Self, M> as yew::Component>::Message,
    ) -> bool;
    /// Returns a `Html` representation of the `VCardPropertyInputGroupObject`.
    fn render(&self, link: &ComponentLink<PropertyGroupInputComponent<Self, M>>) -> Html {
        html! {
            <div class="columns is-mobile is-multiline">
                {
                    for self.get_input_fields(link).iter().map(|field|
                        field.render()
                    )
                }
            </div>
        }
    }
    /// Convenience function for checking if the `VCardPropertyInputGroupObject` is empty.
    fn is_empty(&self) -> bool;
}

/// Type for saving error messages.
///
/// More of a placeholder for something better later on.
#[derive(Debug, Clone, PartialEq)]
pub struct Error {
    pub msg: String,
}
