use crate::model::input_fields::VCardPropertyInputField;
use crate::model::*;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Telephone {
    pub number: String,
    pub work: bool,
    pub home: bool,
    pub text: bool,
    pub voice: bool,
    pub fax: bool,
    pub cell: bool,
    pub video: bool,
    pub pager: bool,
    pub text_phone: bool,
}

#[derive(Clone, PartialEq)]
pub enum TelephoneMsg {
    UpdateNumber(String),
    ToggleWork,
    ToggleHome,
    ToggleText,
    ToggleVoice,
    ToggleFax,
    ToggleCell,
    ToggleVideo,
    TogglePager,
    ToggleTextPhone,

    Generate,
}

impl VCardPropertyInputGroupObject<TelephoneMsg> for Telephone {
    fn new() -> Self {
        Self {
            number: String::new(),
            work: false,
            home: false,
            text: false,
            voice: false,
            fax: false,
            cell: false,
            video: false,
            pager: false,
            text_phone: false,
        }
    }
    fn get_title(&self) -> String {
        "Telephone".to_string()
    }
    fn get_input_fields(
        &self,
        link: &ComponentLink<PropertyGroupInputComponent<Self, TelephoneMsg>>,
    ) -> Vec<VCardPropertyInputField> {
        let typ = String::from("tel");
        vec![
            VCardPropertyInputField::Text {
                label: "Number".to_string(),
                id: Some("number".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| TelephoneMsg::UpdateNumber(e.value)),
                value: self.number.clone(),
                typ,
            },
            VCardPropertyInputField::CheckBox {
                label: "Work".to_string(),
                id: Some("work".to_string()),
                onclick: link.callback(|_: MouseEvent| TelephoneMsg::ToggleWork),
                value: self.work,
            },
            VCardPropertyInputField::CheckBox {
                label: "Home".to_string(),
                id: Some("home".to_string()),
                onclick: link.callback(|_: MouseEvent| TelephoneMsg::ToggleHome),
                value: self.home,
            },
            VCardPropertyInputField::CheckBox {
                label: "Text".to_string(),
                id: Some("text".to_string()),
                onclick: link.callback(|_: MouseEvent| TelephoneMsg::ToggleText),
                value: self.text,
            },
            VCardPropertyInputField::CheckBox {
                label: "Voice".to_string(),
                id: Some("voice".to_string()),
                onclick: link.callback(|_: MouseEvent| TelephoneMsg::ToggleVoice),
                value: self.voice,
            },
            VCardPropertyInputField::CheckBox {
                label: "Fax".to_string(),
                id: Some("fax".to_string()),
                onclick: link.callback(|_: MouseEvent| TelephoneMsg::ToggleFax),
                value: self.fax,
            },
            VCardPropertyInputField::CheckBox {
                label: "Cell".to_string(),
                id: Some("cell".to_string()),
                onclick: link.callback(|_: MouseEvent| TelephoneMsg::ToggleCell),
                value: self.cell,
            },
            VCardPropertyInputField::CheckBox {
                label: "Video".to_string(),
                id: Some("video".to_string()),
                onclick: link.callback(|_: MouseEvent| TelephoneMsg::ToggleVideo),
                value: self.video,
            },
            VCardPropertyInputField::CheckBox {
                label: "Pager".to_string(),
                id: Some("pager".to_string()),
                onclick: link.callback(|_: MouseEvent| TelephoneMsg::TogglePager),
                value: self.pager,
            },
            VCardPropertyInputField::CheckBox {
                label: "Text Phone".to_string(),
                id: Some("text_phone".to_string()),
                onclick: link.callback(|_: MouseEvent| TelephoneMsg::ToggleTextPhone),
                value: self.text_phone,
            },
        ]
    }
    fn update(
        &mut self,
        props: InputProps<Self, TelephoneMsg>,
        msg: <PropertyGroupInputComponent<Self, TelephoneMsg> as yew::Component>::Message,
    ) -> bool {
        match msg {
            TelephoneMsg::UpdateNumber(n) => self.number = n,
            TelephoneMsg::ToggleWork => self.work = !self.work,
            TelephoneMsg::ToggleHome => self.home = !self.home,
            TelephoneMsg::ToggleText => self.text = !self.text,
            TelephoneMsg::ToggleVoice => self.voice = !self.voice,
            TelephoneMsg::ToggleFax => self.fax = !self.fax,
            TelephoneMsg::ToggleCell => self.cell = !self.cell,
            TelephoneMsg::ToggleVideo => self.video = !self.video,
            TelephoneMsg::TogglePager => self.pager = !self.pager,
            TelephoneMsg::ToggleTextPhone => self.text_phone = !self.text_phone,
            TelephoneMsg::Generate => {
                props.generated.emit(self.clone());
            }
        };
        true
    }
    fn is_empty(&self) -> bool {
        self.number.is_empty()
    }
}
