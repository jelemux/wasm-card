use crate::model::input_fields::VCardPropertyInputField;
use crate::model::*;

/// Type that represents the vcard `anniversary` and `birthday` properties.
#[derive(Clone, Debug, Default, PartialEq)]
pub struct OtherIdentification {
    pub nickname: String,
    pub photo: Option<File>,
    pub birthday: String,
    pub anniversary: String,
    pub gender: String,
}

#[derive(Clone, PartialEq)]
pub enum OtherIdentificationMsg {
    UpdateNickname(String),
    UpdatePhoto(Option<File>),
    UpdateBirthday(String),
    UpdateAnniversary(String),
    UpdateGender(String),

    Generate,

    Nope,
}

impl VCardPropertyInputGroupObject<OtherIdentificationMsg> for OtherIdentification {
    fn new() -> Self {
        Self {
            nickname: String::new(),
            photo: None,
            birthday: String::new(),
            anniversary: String::new(),
            gender: String::new(),
        }
    }
    fn get_title(&self) -> std::string::String {
        "Other Identification Properties".to_string()
    }
    fn get_input_fields(
        &self,
        link: &yew::html::Scope<PropertyGroupInputComponent<Self, OtherIdentificationMsg>>,
    ) -> std::vec::Vec<VCardPropertyInputField> {
        vec![
            VCardPropertyInputField::Text {
                label: String::from("Nickname"),
                id: Some(String::from("nickname")),
                placeholder: None,
                oninput: link
                    .callback(|e: InputData| OtherIdentificationMsg::UpdateNickname(e.value)),
                value: self.nickname.clone(),
                typ: String::from("text"),
            },
            VCardPropertyInputField::File {
                label: String::from("Photo"),
                name: String::from("photo"),
                callback: link
                    .callback(|file: Option<File>| OtherIdentificationMsg::UpdatePhoto(file)),
                value: self.photo.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Birthday".to_string(),
                id: Some("birthday".to_string()),
                placeholder: None,
                oninput: link
                    .callback(|e: InputData| OtherIdentificationMsg::UpdateBirthday(e.value)),
                value: self.birthday.clone(),
                typ: String::from("date"),
            },
            VCardPropertyInputField::Text {
                label: "Anniversary".to_string(),
                id: Some("anniversary".to_string()),
                placeholder: None,
                oninput: link
                    .callback(|e: InputData| OtherIdentificationMsg::UpdateAnniversary(e.value)),
                value: self.anniversary.clone(),
                typ: String::from("date"),
            },
            VCardPropertyInputField::Select {
                label: String::from("Gender"),
                id: Some(String::from("gender")),
                options: vec![
                    (String::from(""), String::from("Select one")),
                    (String::from("M"), String::from("Male")),
                    (String::from("F"), String::from("Female")),
                    (String::from("O"), String::from("Other")),
                    (String::from("N"), String::from("Not applicable")),
                    (String::from("U"), String::from("Unknown")),
                ],
                onchange: link.callback(|c: ChangeData| match c {
                    ChangeData::Select(v) => OtherIdentificationMsg::UpdateGender(v.value()),
                    _ => OtherIdentificationMsg::Nope,
                }),
                value: self.gender.clone(),
            },
        ]
    }
    fn update(
        &mut self,
        props: InputProps<Self, OtherIdentificationMsg>,
        msg: <PropertyGroupInputComponent<Self, OtherIdentificationMsg> as yew::Component>::Message,
    ) -> bool {
        match msg {
            OtherIdentificationMsg::UpdateNickname(n) => self.nickname = n,
            OtherIdentificationMsg::UpdatePhoto(p) => self.photo = p,
            OtherIdentificationMsg::UpdateBirthday(b) => self.birthday = b,
            OtherIdentificationMsg::UpdateAnniversary(a) => self.anniversary = a,
            OtherIdentificationMsg::UpdateGender(g) => self.gender = g,
            OtherIdentificationMsg::Generate => props.generated.emit(self.clone()),
            OtherIdentificationMsg::Nope => (),
        };
        true
    }
    fn is_empty(&self) -> bool {
        self.anniversary.is_empty() && self.birthday.is_empty()
    }
}
