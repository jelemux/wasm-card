use crate::model::input_fields::VCardPropertyInputField;
use crate::model::*;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Address {
    pub post_office_box: String,
    pub extension: String,
    pub street: String,
    pub locality: String,
    pub region: String,
    pub code: String,
    pub country: String,
    pub work: bool,
    pub home: bool,
}

#[derive(Clone, PartialEq)]
pub enum AddressMsg {
    UpdatePostOfficeBox(String),
    UpdateExtension(String),
    UpdateStreet(String),
    UpdateLocality(String),
    UpdateRegion(String),
    UpdateCode(String),
    UpdateCountry(String),
    ToggleWork,
    ToggleHome,

    Generate,
}

impl VCardPropertyInputGroupObject<AddressMsg> for Address {
    fn new() -> Self {
        Self {
            post_office_box: String::new(),
            extension: String::new(),
            street: String::new(),
            locality: String::new(),
            region: String::new(),
            code: String::new(),
            country: String::new(),
            work: false,
            home: false,
        }
    }
    fn get_title(&self) -> String {
        "Address".to_string()
    }
    fn get_input_fields(
        &self,
        link: &ComponentLink<PropertyGroupInputComponent<Self, AddressMsg>>,
    ) -> Vec<VCardPropertyInputField> {
        let typ = String::from("text");
        vec![
            VCardPropertyInputField::Text {
                label: "Post Office Box".to_string(),
                id: Some("post_office_box".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| AddressMsg::UpdatePostOfficeBox(e.value)),
                value: self.post_office_box.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Extension".to_string(),
                id: Some("extension".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| AddressMsg::UpdateExtension(e.value)),
                value: self.extension.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Street".to_string(),
                id: Some("street".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| AddressMsg::UpdateStreet(e.value)),
                value: self.street.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Locality".to_string(),
                id: Some("locality".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| AddressMsg::UpdateLocality(e.value)),
                value: self.locality.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Region".to_string(),
                id: Some("region".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| AddressMsg::UpdateRegion(e.value)),
                value: self.region.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Code".to_string(),
                id: Some("code".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| AddressMsg::UpdateCode(e.value)),
                value: self.code.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Country".to_string(),
                id: Some("country".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| AddressMsg::UpdateCountry(e.value)),
                value: self.country.clone(),
                typ,
            },
            VCardPropertyInputField::CheckBox {
                label: "Work".to_string(),
                id: Some("work".to_string()),
                onclick: link.callback(|_: MouseEvent| AddressMsg::ToggleWork),
                value: self.work,
            },
            VCardPropertyInputField::CheckBox {
                label: "Home".to_string(),
                id: Some("home".to_string()),
                onclick: link.callback(|_: MouseEvent| AddressMsg::ToggleHome),
                value: self.home,
            },
        ]
    }
    fn update(
        &mut self,
        props: InputProps<Self, AddressMsg>,
        msg: <PropertyGroupInputComponent<Self, AddressMsg> as yew::Component>::Message,
    ) -> bool {
        match msg {
            AddressMsg::UpdatePostOfficeBox(b) => self.post_office_box = b,
            AddressMsg::UpdateExtension(e) => self.extension = e,
            AddressMsg::UpdateStreet(s) => self.street = s,
            AddressMsg::UpdateLocality(l) => self.locality = l,
            AddressMsg::UpdateRegion(r) => self.region = r,
            AddressMsg::UpdateCode(p) => self.code = p,
            AddressMsg::UpdateCountry(c) => self.country = c,
            AddressMsg::ToggleWork => self.work = !self.work,
            AddressMsg::ToggleHome => self.home = !self.home,
            AddressMsg::Generate => {
                props.generated.emit(self.clone());
            }
        };
        true
    }
    fn is_empty(&self) -> bool {
        self.post_office_box.is_empty()
            && self.extension.is_empty()
            && self.street.is_empty()
            && self.locality.is_empty()
            && self.region.is_empty()
            && self.code.is_empty()
            && self.country.is_empty()
    }
}
