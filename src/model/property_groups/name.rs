use crate::model::input_fields::VCardPropertyInputField;
use crate::model::*;

/// Type that represents a vcard `name` property
///
/// # Examples
/// ```
/// # use bcard_wasm_webapp::viewmodel::name::Name;
/// # use crate::bcard_wasm_webapp::viewmodel::VCardPropertyInputObject;
/// let mut name = Name::new();
/// name.prefix = String::from("Sir");
/// name.first_name = String::from("Arthur");
/// name.middle_name = String::from("Charles");
/// name.last_name = String::from("Clarke");
/// name.suffix = String::from("CBE FRAS");
///
/// assert_eq!(name.generate_fn(), String::from("Sir Arthur Charles Clarke, CBE FRAS"));
/// ```
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Name {
    pub prefix: String,
    pub first_name: String,
    pub middle_name: String,
    pub last_name: String,
    pub suffix: String,
}

#[derive(Clone, PartialEq)]
pub enum NameMsg {
    UpdatePrefix(String),
    UpdateFirstName(String),
    UpdateMiddleName(String),
    UpdateLastName(String),
    UpdateSuffix(String),

    Generate,
}

impl VCardPropertyInputGroupObject<NameMsg> for Name {
    fn new() -> Self {
        Self {
            prefix: String::new(),
            first_name: String::new(),
            middle_name: String::new(),
            last_name: String::new(),
            suffix: String::new(),
        }
    }
    fn get_title(&self) -> String {
        "Name".to_string()
    }
    fn get_input_fields(
        &self,
        link: &ComponentLink<PropertyGroupInputComponent<Self, NameMsg>>,
    ) -> std::vec::Vec<VCardPropertyInputField> {
        let typ = String::from("text");
        vec![
            VCardPropertyInputField::Text {
                label: "Prefix".to_string(),
                id: Some("prefix".to_string()),
                placeholder: Some("Sir".to_string()),
                oninput: link.callback(|e: InputData| NameMsg::UpdatePrefix(e.value)),
                value: self.prefix.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "First Name".to_string(),
                id: Some("first_name".to_string()),
                placeholder: Some("Arthur".to_string()),
                oninput: link.callback(|e: InputData| NameMsg::UpdateFirstName(e.value)),
                value: self.first_name.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Middle Name".to_string(),
                id: Some("middle_name".to_string()),
                placeholder: Some("Charles".to_string()),
                oninput: link.callback(|e: InputData| NameMsg::UpdateMiddleName(e.value)),
                value: self.middle_name.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Last Name".to_string(),
                id: Some("last_name".to_string()),
                placeholder: Some("Clarke".to_string()),
                oninput: link.callback(|e: InputData| NameMsg::UpdateLastName(e.value)),
                value: self.last_name.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Suffix".to_string(),
                id: Some("suffix".to_string()),
                placeholder: Some("CBE FRAS".to_string()),
                oninput: link.callback(|e: InputData| NameMsg::UpdateSuffix(e.value)),
                value: self.suffix.clone(),
                typ,
            },
        ]
    }
    fn update(
        &mut self,
        props: InputProps<Self, NameMsg>,
        msg: <PropertyGroupInputComponent<Self, NameMsg> as yew::Component>::Message,
    ) -> bool {
        match msg {
            NameMsg::UpdatePrefix(p) => self.prefix = p,
            NameMsg::UpdateFirstName(f) => self.first_name = f,
            NameMsg::UpdateMiddleName(m) => self.middle_name = m,
            NameMsg::UpdateLastName(l) => self.last_name = l,
            NameMsg::UpdateSuffix(s) => self.suffix = s,
            NameMsg::Generate => {
                props.generated.emit(self.clone());
            }
        };
        true
    }
    fn is_empty(&self) -> bool {
        self.prefix.is_empty()
            && self.first_name.is_empty()
            && self.middle_name.is_empty()
            && self.last_name.is_empty()
            && self.suffix.is_empty()
    }
}

impl Name {
    pub fn generate_fn(&self) -> String {
        let mut full_name = String::new();

        full_name.push_str(&self.prefix);
        if !self.first_name.is_empty() {
            full_name.push_str(" ");
            full_name.push_str(&self.first_name);
        }
        if !self.middle_name.is_empty() {
            full_name.push_str(" ");
            full_name.push_str(&self.middle_name);
        }
        if !self.last_name.is_empty() {
            full_name.push_str(" ");
            full_name.push_str(&self.last_name);
        }
        if !self.suffix.is_empty() {
            full_name.push_str(", ");
            full_name.push_str(&self.suffix);
        }

        full_name
    }
}
