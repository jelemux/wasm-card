use crate::model::input_fields::VCardPropertyInputField;
use crate::model::*;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Organizational {
    pub org: String,
    pub logo: Option<File>,
    pub title: String,
    pub role: String,
    pub member: String,
    pub related: String,
}

#[derive(Clone, PartialEq)]
pub enum OrganizationalMsg {
    UpdateOrg(String),
    UpdateLogo(Option<File>),
    UpdateTitle(String),
    UpdateRole(String),
    UpdateMember(String),
    UpdateRelated(String),

    Generate,
}

impl VCardPropertyInputGroupObject<OrganizationalMsg> for Organizational {
    fn new() -> Self {
        Self {
            org: String::new(),
            logo: None,
            title: String::new(),
            role: String::new(),
            member: String::new(),
            related: String::new(),
        }
    }
    fn get_title(&self) -> std::string::String {
        "Organizational".to_string()
    }
    fn get_input_fields(
        &self,
        link: &yew::html::Scope<PropertyGroupInputComponent<Self, OrganizationalMsg>>,
    ) -> std::vec::Vec<VCardPropertyInputField> {
        let typ = String::from("text");
        vec![
            VCardPropertyInputField::Text {
                label: "Organisation".to_string(),
                id: Some("org".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| OrganizationalMsg::UpdateOrg(e.value)),
                value: self.org.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::File {
                // TODO: Add Upload for logo
                label: "Logo".to_string(),
                name: "logo".to_string(),
                callback: link.callback(|file: Option<File>| OrganizationalMsg::UpdateLogo(file)),
                value: self.logo.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Title".to_string(),
                id: Some("title".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| OrganizationalMsg::UpdateTitle(e.value)),
                value: self.title.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Role".to_string(),
                id: Some("role".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| OrganizationalMsg::UpdateRole(e.value)),
                value: self.role.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Member".to_string(),
                id: Some("member".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| OrganizationalMsg::UpdateMember(e.value)),
                value: self.member.clone(),
                typ: typ.clone(),
            },
            VCardPropertyInputField::Text {
                label: "Related".to_string(),
                id: Some("related".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| OrganizationalMsg::UpdateRelated(e.value)),
                value: self.related.clone(),
                typ: typ,
            },
        ]
    }
    fn update(
        &mut self,
        props: InputProps<Self, OrganizationalMsg>,
        msg: <PropertyGroupInputComponent<Self, OrganizationalMsg> as yew::Component>::Message,
    ) -> bool {
        match msg {
            OrganizationalMsg::UpdateOrg(o) => self.org = o,
            OrganizationalMsg::UpdateLogo(l) => self.logo = l,
            OrganizationalMsg::UpdateTitle(t) => self.title = t,
            OrganizationalMsg::UpdateRole(r) => self.role = r,
            OrganizationalMsg::UpdateMember(m) => self.member = m,
            OrganizationalMsg::UpdateRelated(r) => self.related = r,
            OrganizationalMsg::Generate => {
                props.generated.emit(self.clone());
            }
        };
        true
    }
    fn is_empty(&self) -> bool {
        self.org.is_empty()
            && self.logo.is_none()
            && self.title.is_empty()
            && self.role.is_empty()
            && self.member.is_empty()
            && self.related.is_empty()
    }
}
