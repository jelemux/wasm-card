use crate::model::input_fields::VCardPropertyInputField;
use crate::model::*;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Communication {
    pub email_address: String,
    pub impp: String,
}

#[derive(Clone, PartialEq)]
pub enum CommunicationMsg {
    UpdateEmailAddress(String),
    UpdateImpp(String),

    Generate,
}

impl VCardPropertyInputGroupObject<CommunicationMsg> for Communication {
    fn new() -> Self {
        Self {
            email_address: String::new(),
            impp: String::new(),
        }
    }
    fn get_title(&self) -> String {
        String::from("Communication")
    }
    fn get_input_fields(
        &self,
        link: &yew::html::Scope<PropertyGroupInputComponent<Self, CommunicationMsg>>,
    ) -> Vec<VCardPropertyInputField> {
        vec![
            VCardPropertyInputField::Text {
                label: "Email Address".to_string(),
                id: Some("email_address".to_string()),
                placeholder: None,
                oninput: link
                    .callback(|e: InputData| CommunicationMsg::UpdateEmailAddress(e.value)),
                value: self.email_address.clone(),
                typ: String::from("email"),
            },
            VCardPropertyInputField::Text {
                label: "Instant Messaging URI".to_string(),
                id: Some("impp".to_string()),
                placeholder: None,
                oninput: link.callback(|e: InputData| CommunicationMsg::UpdateImpp(e.value)),
                value: self.impp.clone(),
                typ: String::from("text"),
            },
        ]
    }
    fn update(
        &mut self,
        props: InputProps<Self, CommunicationMsg>,
        msg: <PropertyGroupInputComponent<Self, CommunicationMsg> as yew::Component>::Message,
    ) -> bool {
        match msg {
            CommunicationMsg::UpdateEmailAddress(a) => self.email_address = a,
            CommunicationMsg::UpdateImpp(i) => self.impp = i,

            CommunicationMsg::Generate => {
                props.generated.emit(self.clone());
            }
        };

        true
    }
    fn is_empty(&self) -> bool {
        self.email_address.is_empty() && self.impp.is_empty()
    }
}
