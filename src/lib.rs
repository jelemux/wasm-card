#![recursion_limit = "2048"]
extern crate console_error_panic_hook;
extern crate wee_alloc;
use std::panic;
use view::main::MainView;
use wasm_bindgen::prelude::*;
use yew::prelude::App;

// Use `wee_alloc` as the global allocator.
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

pub mod model;
pub mod view;

fn init() {
    panic::set_hook(Box::new(console_error_panic_hook::hook));
}

#[wasm_bindgen(start)]
pub fn run_app() {
    init();
    App::<MainView>::new().mount_to_body();
}
