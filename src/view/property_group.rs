use crate::model::*;
use crate::view::weak_links::WeakComponentLink;
use yew::prelude::*;
use yewtil::NeqAssign;

#[derive(Clone, PartialEq, Properties)]
pub struct InputProps<
    O: 'static + VCardPropertyInputGroupObject<M> + Clone,
    M: 'static + PartialEq + Clone,
> {
    pub generated: Callback<O>,
    pub delete: Callback<()>,
    pub weak_link: WeakComponentLink<PropertyGroupInputComponent<O, M>>,
}

#[derive(Clone, PartialEq)]
pub struct PropertyGroupInputComponent<
    O: 'static + VCardPropertyInputGroupObject<M>,
    M: 'static + PartialEq + Clone,
> {
    pub props: InputProps<O, M>,
    pub value: O,
    pub error: Option<Error>,
}

impl<O: 'static + VCardPropertyInputGroupObject<M>, M: 'static + PartialEq + Clone> Component
    for PropertyGroupInputComponent<O, M>
{
    type Message = M;
    type Properties = InputProps<O, M>;
    fn create(props: <Self as yew::Component>::Properties, link: yew::html::Scope<Self>) -> Self {
        props.weak_link.borrow_mut().replace(link);
        Self {
            props,
            value: O::new(),
            error: None,
        }
    }
    fn update(&mut self, msg: <Self as yew::Component>::Message) -> bool {
        self.value.update(self.props.clone(), msg)
    }
    fn change(&mut self, props: <Self as yew::Component>::Properties) -> bool {
        self.props.neq_assign(props)
    }
    fn view(&self) -> yew::virtual_dom::VNode {
        let link = self.props.weak_link.borrow().clone().unwrap();
        let delete = self.props.delete.clone();

        html! {
            <div class="box">
                { self.render_error() }

                <div class="level">
                    <div class="level-left">
                        <h3 class="subtitle">{ self.value.get_title() }</h3>
                    </div>
                    <div class="level-right">
                        <button onclick=Callback::once(move |_| delete.emit(())) class="delete"></button>
                    </div>
                </div>

                { self.value.render(&link) }

            </div>
        }
    }
}

impl<O: VCardPropertyInputGroupObject<M>, M: 'static + PartialEq + Clone>
    PropertyGroupInputComponent<O, M>
{
    /// Returns the error as `Html`
    fn render_error(&self) -> Html {
        html! {
            <>
                {
                    if self.error.is_some() {
                        html!{
                            <div class="notification is-danger is-light">
                                { self.error.clone().unwrap().msg }
                            </div>
                        }
                    } else {
                        html!{}
                    }
                }
            </>
        }
    }
}
