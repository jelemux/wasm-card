use crate::model::property_groups::address::*;
use crate::model::property_groups::communication::*;
use crate::model::property_groups::name::*;
use crate::model::property_groups::organizational::*;
use crate::model::property_groups::other_identification::*;
use crate::model::property_groups::telephone::*;
use crate::model::utility::*;
use crate::model::vcard::VCardData;
use crate::model::Error;
use crate::view::property_group::PropertyGroupInputComponent;
use crate::view::weak_links::WeakComponentLink;
use qrcodegen::QrCode;
use qrcodegen::QrCodeEcc;
use yew::prelude::*;
use yew::services::ConsoleService;
use yewtil::ptr::Mrc;

type NameView = PropertyGroupInputComponent<Name, NameMsg>;
type AddressView = PropertyGroupInputComponent<Address, AddressMsg>;
type OtherIdentificationView =
    PropertyGroupInputComponent<OtherIdentification, OtherIdentificationMsg>;
type OrganizationalView = PropertyGroupInputComponent<Organizational, OrganizationalMsg>;
type TelephoneView = PropertyGroupInputComponent<Telephone, TelephoneMsg>;
type CommunicationView = PropertyGroupInputComponent<Communication, CommunicationMsg>;

pub struct MainView {
    link: ComponentLink<Self>,
    error: Option<Error>,
    download: Option<Download>,
    selected_option: DownloadOption,
    vcard_data: Mrc<VCardData>,

    name_links: Vec<WeakComponentLink<NameView>>,
    address_links: Vec<WeakComponentLink<AddressView>>,
    telephone_links: Vec<WeakComponentLink<TelephoneView>>,
    communcation_links: Vec<WeakComponentLink<CommunicationView>>,
    other_identifications_links: Vec<WeakComponentLink<OtherIdentificationView>>,
    organizational_links: Vec<WeakComponentLink<OrganizationalView>>,

    answer_count: usize,
}

pub enum Msg {
    AddName,
    DelName(usize),
    AddAddress,
    DelAddress(usize),
    AddTelephone,
    DelTelephone(usize),
    AddOtherIdentification,
    DelOtherIdentification(usize),
    AddOrganizational,
    DelOrganizational(usize),
    AddCommunication,
    DelCommunication(usize),

    ChangeDownloadOption(DownloadOption),

    Generate,
    GeneratedName(Name),
    GeneratedOtherIdentification(OtherIdentification),
    GeneratedAddress(Address),
    GeneratedTelephone(Telephone),
    GeneratedOrganizational(Organizational),
    GeneratedCommunication(Communication),
    GenerationComplete,

    Nope,
}

impl Component for MainView {
    type Message = Msg;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        MainView {
            link,
            error: None,
            download: None,
            selected_option: DownloadOption::VCard,
            vcard_data: Mrc::new(VCardData::new()),

            name_links: vec![WeakComponentLink::default()],
            address_links: vec![WeakComponentLink::default()],
            telephone_links: vec![WeakComponentLink::default()],
            communcation_links: vec![WeakComponentLink::default()],
            other_identifications_links: vec![WeakComponentLink::default()],
            organizational_links: vec![WeakComponentLink::default()],
            answer_count: 0,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        self.error = None;

        let shouldrender = match msg {
            Msg::AddName => {
                self.name_links.push(WeakComponentLink::default());
                true
            }
            Msg::DelName(idx) => {
                self.name_links.remove(idx);
                true
            }
            Msg::AddAddress => {
                self.address_links.push(WeakComponentLink::default());
                true
            }
            Msg::DelAddress(idx) => {
                self.address_links.remove(idx);
                true
            }
            Msg::AddTelephone => {
                self.telephone_links.push(WeakComponentLink::default());
                true
            }
            Msg::DelTelephone(idx) => {
                self.telephone_links.remove(idx);
                true
            }
            Msg::AddOtherIdentification => {
                self.other_identifications_links
                    .push(WeakComponentLink::default());
                true
            }
            Msg::DelOtherIdentification(idx) => {
                self.other_identifications_links.remove(idx);
                true
            }
            Msg::AddOrganizational => {
                self.organizational_links.push(WeakComponentLink::default());
                true
            }
            Msg::DelOrganizational(idx) => {
                self.organizational_links.remove(idx);
                true
            }
            Msg::AddCommunication => {
                self.communcation_links.push(WeakComponentLink::default());
                true
            }
            Msg::DelCommunication(idx) => {
                self.communcation_links.remove(idx);
                true
            }
            Msg::ChangeDownloadOption(option) => {
                self.selected_option = option;
                false
            }
            Msg::Generate => {
                
                for name_link in self.name_links.iter() {
                    let name_link = name_link.borrow().clone().unwrap();
                    name_link.send_message(NameMsg::Generate);
                }

                for address_link in self.address_links.iter() {
                    let address_link = address_link.borrow().clone().unwrap();
                    address_link.send_message(AddressMsg::Generate);
                }

                for telephone_link in self.telephone_links.iter() {
                    let telephone_link = telephone_link.borrow().clone().unwrap();
                    telephone_link.send_message(TelephoneMsg::Generate);
                }

                for other_identifications_link in self.other_identifications_links.iter() {
                    let other_identifications_link =
                        other_identifications_link.borrow().clone().unwrap();
                    other_identifications_link.send_message(OtherIdentificationMsg::Generate)
                }

                for organizational_link in self.organizational_links.iter() {
                    let organizational_link = organizational_link.borrow().clone().unwrap();
                    organizational_link.send_message(OrganizationalMsg::Generate)
                }

                for communcation_link in self.communcation_links.iter() {
                    let communcation_link = communcation_link.borrow().clone().unwrap();
                    communcation_link.send_message(CommunicationMsg::Generate)
                }

                true
            }
            Msg::GeneratedName(name) => {
                self.answer_count += 1;

                match self.vcard_data.get_mut() {
                    Some(vcard_data) => vcard_data.add_name(name),
                    None => ConsoleService::info(
                        "Error in GeneratedName: Couldn't get mutable borrow of VCardData",
                    ),
                };

                true
            }
            Msg::GeneratedOtherIdentification(other_identification) => {
                self.answer_count += 1;

                match self.vcard_data.get_mut() {
                    Some(vcard_data) => vcard_data.add_other_identification(other_identification),
                    None => ConsoleService::info(
                        "Error in GeneratedOtherIdentification: Couldn't get mutable borrow of VCardData",
                    ),
                };

                true
            }
            Msg::GeneratedAddress(address) => {
                self.answer_count += 1;

                match self.vcard_data.get_mut() {
                    Some(vcard_data) => vcard_data.add_address(address),
                    None => ConsoleService::info(
                        "Error in GeneratedAddress: Couldn't get mutable borrow of VCardData",
                    ),
                };

                true
            }
            Msg::GeneratedTelephone(telephone) => {
                self.answer_count += 1;

                match self.vcard_data.get_mut() {
                    Some(vcard_data) => vcard_data.add_telephone(telephone),
                    None => ConsoleService::info(
                        "Error in GeneratedTelephone: Couldn't get mutable borrow of VCardData",
                    ),
                };

                true
            }
            Msg::GeneratedCommunication(communication) => {
                self.answer_count += 1;

                match self.vcard_data.get_mut() {
                    Some(vcard_data) => vcard_data.add_communication(communication),
                    None => ConsoleService::info(
                        "Error in GeneratedCommunication: Couldn't get mutable borrow of VCardData",
                    ),
                };

                true
            }
            Msg::GeneratedOrganizational(organizational) => {
                self.answer_count += 1;

                match self.vcard_data.get_mut() {
                    Some(vcard_data) => vcard_data.add_organizational(organizational),
                    None => ConsoleService::info("Error in GeneratedOrganizational: Couldn't get mutable borrow of VCardData"),
                };

                true
            }
            Msg::GenerationComplete => {
                self.answer_count = 0;

                if let DownloadOption::PDF = self.selected_option {
                    match self.vcard_data.generate_pdf() {
                        Ok(pdf) => {
                            let mut vcard_name = String::new();
                            if let Some(vcard_data) = self.vcard_data.get_mut() {
                                if let Some(name) = vcard_data.names.get(0) {
                                    vcard_name = name.generate_fn();
                                }
                            }

                            self.download = Some(
                                Download {
                                    file_name: format!("Visitenkarten {}.pdf", vcard_name),
                                    content: pdf,
                                    mime_type: MimeType::PDF,
                                }
                            );
                        },
                        Err(_) => self.error = Some(
                            Error { 
                                msg: String::from("Unexpected error while generating the PDF. Please contact me about it.")
                            }
                        ),
                    };
                } else {
                    match self.vcard_data.build_vcard() {
                        Ok(vcard) => {
                            match self.selected_option {
                                DownloadOption::VCard => {
                                    self.download = Some(Download {
                                        file_name: String::from("VCard.vcs"),
                                        content: vobject::write_component(&vcard),
                                        mime_type: MimeType::VCard,
                                    });
                                }
                                DownloadOption::QrCode => {
                                    match QrCode::encode_text(
                                        &vobject::write_component(&vcard),
                                        QrCodeEcc::Low,
                                    ) {
                                        Ok(qr) => {
                                            self.download = Some(Download {
                                                file_name: String::from("QR-Code VCard.svg"),
                                                content: qr.to_svg_string(4),
                                                mime_type: MimeType::SVG,
                                            })
                                        }
                                        Err(_) => {
                                            self.error = Some(Error {
                                                msg: String::from("Sorry, VCard is too long!"),
                                            })
                                        }
                                    };
                                }
                                _ => (),
                            };
                        }
                        Err(err) => {
                            self.error = Some(Error {
                                msg: err.to_string(),
                            })
                        }
                    };
                }

                match self.vcard_data.get_mut() {
                    Some(vcard_data) => *vcard_data = VCardData::new(),
                    None => ConsoleService::info("Couldn't reset VCardData"),
                };

                true
            }
            Msg::Nope => false,
        };

        if self.answer_count >= self.get_subcomponent_count() {
            self.link.send_message(Msg::GenerationComplete);
        }
        if self.error.is_some() {
            self.download = None;
        }

        shouldrender
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let download_options = self.link.callback(|e: ChangeData| match e {
            ChangeData::Select(v) => match v.value().as_str() {
                "vcard" => Msg::ChangeDownloadOption(DownloadOption::VCard),
                "pdf" => Msg::ChangeDownloadOption(DownloadOption::PDF),
                "qrcode" => Msg::ChangeDownloadOption(DownloadOption::QrCode),
                _ => Msg::Nope,
            },
            _ => Msg::Nope,
        });

        html! {
            <>
                <main>
                    <section class="hero">
                        <div class="hero-body">
                            <div class="container is-max-widescreen">
                                <h1 class="title">{ "A Generator for vCards" }</h1>
                                <h2 class="subtitle">{ "Supports generating vCards (.vcf), print-ready PDF business cards and QR Codes" }</h2>
                            </div>
                        </div>
                    </section>

                    <section class="section">
                        <div class="container is-max-widescreen">

                            { self.render_error() }

                            <div class="level">
                                <div class="level-left">
                                    <div class="level-item">
                                        <h2 class="subtitle is-2">{ "Names" }</h2>
                                    </div>
                                    <div class="level-item">
                                        <button onclick=self.link.callback(|_| Msg::AddName) class="button">{ "Add more" }</button>
                                    </div>
                                </div>
                            </div>

                            {
                                for self.name_links.iter().enumerate().map(move |(idx, link)|
                                    html!{
                                        <NameView   weak_link=link
                                                    generated=self.link.callback(
                                                        |n: Name|
                                                            Msg::GeneratedName(n)
                                                    )
                                                    delete=self.link.callback(move |_| Msg::DelName(idx))
                                        />
                                    }
                                )
                            }

                            <div class="level">
                                <div class="level-left">
                                    <div class="level-item">
                                        <h2 class="subtitle is-2">{ "Other Identification Properties" }</h2>
                                    </div>
                                    <div class="level-item">
                                        <button onclick=self.link.callback(|_| Msg::AddOtherIdentification) class="button">{ "Add more" }</button>
                                    </div>
                                </div>
                            </div>

                            {
                                for self.other_identifications_links.iter().enumerate().map(move |(idx, link)|
                                    html!{
                                        <OtherIdentificationView  weak_link=link
                                                    generated=self.link.callback(
                                                        |d: OtherIdentification|
                                                            Msg::GeneratedOtherIdentification(d)
                                                    )
                                                    delete=self.link.callback(move |_| Msg::DelOtherIdentification(idx))
                                        />
                                    }
                                )
                            }

                            <div class="level">
                                <div class="level-left">
                                    <div class="level-item">
                                        <h2 class="subtitle is-2">{ "Addresses" }</h2>
                                    </div>
                                    <div class="level-item">
                                        <button onclick=self.link.callback(|_| Msg::AddAddress) class="button">{ "Add more" }</button>
                                    </div>
                                </div>
                            </div>

                            {
                                for self.address_links.iter().enumerate().map(move |(idx, link)|
                                    html!{
                                        <AddressView    weak_link=link
                                                        generated=self.link.callback(
                                                            |a: Address|
                                                                Msg::GeneratedAddress(a)
                                                        )
                                                        delete=self.link.callback(move |_| Msg::DelAddress(idx))
                                        />
                                    }
                                )
                            }

                            <div class="level">
                                <div class="level-left">
                                    <div class="level-item">
                                        <h2 class="subtitle is-2">{ "Telephone Numbers" }</h2>
                                    </div>
                                    <div class="level-item">
                                        <button onclick=self.link.callback(|_| Msg::AddTelephone) class="button">{ "Add more" }</button>
                                    </div>
                                </div>
                            </div>

                            {
                                for self.telephone_links.iter().enumerate().map(move |(idx, link)|
                                    html!{
                                        <TelephoneView  weak_link=link
                                                        generated=self.link.callback(
                                                            |t: Telephone|
                                                                Msg::GeneratedTelephone(t)
                                                        )
                                                        delete=self.link.callback(move |_| Msg::DelTelephone(idx))
                                        />
                                    }
                                )
                            }

                            <div class="level">
                                <div class="level-left">
                                    <div class="level-item">
                                        <h2 class="subtitle is-2">{ "Communication" }</h2>
                                    </div>
                                    <div class="level-item">
                                        <button onclick=self.link.callback(|_| Msg::AddCommunication) class="button">{ "Add more" }</button>
                                    </div>
                                </div>
                            </div>

                            {
                                for self.communcation_links.iter().enumerate().map(move |(idx, link)|
                                    html!{
                                        <CommunicationView  weak_link=link
                                                        generated=self.link.callback(
                                                            |c: Communication|
                                                                Msg::GeneratedCommunication(c)
                                                        )
                                                        delete=self.link.callback(move |_| Msg::DelCommunication(idx))
                                        />
                                    }
                                )
                            }

                            <div class="level">
                                <div class="level-left">
                                    <div class="level-item">
                                        <h2 class="subtitle is-2">{ "Organisations" }</h2>
                                    </div>
                                    <div class="level-item">
                                        <button onclick=self.link.callback(|_| Msg::AddOrganizational) class="button">{ "Add more" }</button>
                                    </div>
                                </div>
                            </div>

                            {
                                for self.organizational_links.iter().enumerate().map(move |(idx, link)|
                                    html!{
                                        <OrganizationalView weak_link=link
                                                            generated=self.link.callback(
                                                                |o: Organizational|
                                                                    Msg::GeneratedOrganizational(o)
                                                            )
                                                            delete=self.link.callback(move |_| Msg::DelOrganizational(idx))
                                        />
                                    }
                                )
                            }

                            <div class="block level-left">
                                <button onclick=self.link.callback(|_| Msg::Generate) class="button is-primary level-item">{ "Generate" }</button>

                                <div class="select level-item">
                                    <select id="download_options" onchange=download_options>
                                        <option value="vcard">{ "VCard (.vcf)" }</option>
                                        <option value="pdf">{ "Print-ready PDF" }</option>
                                        <option value="qrcode">{ "QR Code" }</option>
                                    </select>
                                </div>

                                { self.render_download() }
                            </div>

                            <div class="block">
                                { self.render_preview() }
                            </div>

                        </div>
                    </section>
                </main>

                <footer class="footer">
                    <div class="content has-text-centered">
                        <p>
                            <strong>{ "VCard Generator" }</strong> { " by " } <a href="https://jelemux.dev">{ "Jeremias Weber" }</a>{ ". "}
                            { "The source code is licenced " } <a href="http://opensource.org/licenses/mit-license.php">{ "MIT" }</a>{"."}
                        </p>
                    </div>
                </footer>
            </>
        }
    }
}

impl MainView {

    fn render_error(&self) -> Html {
        html! {
            <>
                {
                    match &self.error {
                        Some(error) => {
                            html!{
                                <div class="notification is-danger is-light">
                                    { error.msg.clone() }
                                </div>
                            }
                        },
                        None => html!{},
                    }
                }
            </>
        }
    }

    fn render_download(&self) -> Html {
        if self.download.is_some() {
            let download = self.download.as_ref().unwrap();

            html! {
                <a href=download.as_data_link() download=download.file_name class="button is-success level-item" >
                    { "Download" }
                </a>
            }
        } else {
            html! {}
        }
    }

    fn render_preview(&self) -> Html {
        if self.download.is_some() {
            let download = self.download.as_ref().unwrap();

            match download.mime_type {
                MimeType::PDF => html! {
                    <iframe src=download.as_data_link() alt="PDF Preview" width="400" height="550"/>
                },
                MimeType::VCard => {
                    html! {
                        <pre>
                            <code> { download.content.clone() } </code>
                        </pre>
                    }
                }
                MimeType::SVG => html! {
                    <img src=download.as_data_link() alt="Image Preview" class="image is-square" width="300" height="300"/>
                },
            }
        } else {
            html! {}
        }
    }

    fn get_subcomponent_count(&self) -> usize {
        self.name_links.len()
            + self.address_links.len()
            + self.telephone_links.len()
            + self.other_identifications_links.len()
            + self.organizational_links.len()
            + self.communcation_links.len()
    }
}
