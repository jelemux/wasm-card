# Rust

Bei Rust handelt es sich um eine sogenannte Systemprogrammiersprache (engl.: systems programming language). Solche Sprachen zeichnen sich dadurch aus, dass sich mit ihnen Betriebssysteme und andere eng mit der Hardware verbundene Anwendungen realisieren lassen.

2010 bei Mozilla entstanden, wurde die Sprache zunächst als Hobby-Projekt des Mozilla-Entwicklers Graydon Hoare entworfen und dann eingesetzt, um eine neue Browser-Engine für Firefox zu entwickeln. Seit einiger Zeit ist das Projekt Open Source und es wurde mittlerweile sogar eine Rust Foundation gegründet, die sich unabhängig von Mozilla um das Weiterführen des Projekts kümmert.

## Sprachdesign

### Syntax

Auch wenn sich Rust bei der grundsätzlichen Syntax an C orientiert (Semikolon zum Trennen von Anweisungen, geschweifte Klammern für Blöcke) unterscheidet es sich ansonsten doch sehr davon.  
Beispielsweise werden keine Klammern um die Bedingungnen von `if`- und `while`-Statements benötigt, es gibt keine herkömmlichen `for`-Schleifen, sondern nur Foreach-Schleifen die auch das Iterieren über selbst definierte Datenstrukturen ermöglichen (Stichwort [Iterator-Trait](https://doc.rust-lang.org/std/iter/trait.Iterator.html)). Desweiteren ermöglichen die sehr mächtigen `match`-Ausdrücke Pattern Matching nicht nur mit Zahlen und Zeichenketten, sondern mit beliebig verschachtelten Strukturen.

Variablen werden in `snake_case` mit dem Schlüsselwort `let` definiert, komplexe Datentypen in `PascalCase`. Generische Typparameter stehen in spitzen Klammern (`<>`). Zwei Doppelpunkte (`::`) geben die Herkunft eines Bezeichners aus einem Namensraum an.

### Typsystem

Eigene Datentypen können als `struct` oder als `enum` definiert werden, welches sich allerdings nicht wie ein Enum in beispielsweise Java verhält sondern eigentlich dem viel mächtigeren Tagged-Union entspricht. Mit Hilfe von `impl` lassen sich für beide dieser Datentypen Methoden definieren. Falls die Methoden jedoch nicht statisch sein sollen, müssen sie eine Referenz auf `self` als Parameter annehmen.

Vererbung wurde in Rust absichtlich nicht definiert. Stattdessen wird die Komposition empfohlen. Polymorphie ist durch die den Interfaces ähnlichen aber viel mächtigeren Traits und generische Programmierung möglich. Traits können Funktionen definieren, die von einem Datentyp implementiert werden sollen, sie können aber auch schon eine Default-Implementierung liefern. So ist es zum Beispiel möglich, dass für den `Iterator`-Trait nur die Methode `next(&mut self)` implementiert werden muss und alle anderen Methoden wie z.B. `count(self)` oder `last(self)` daraus abgeleitet werden. Auch die Operatoren wie beispielsweise `+` lassen sich von eigenen Datentypen nutzen, indem Traits wie der `Add`-Trait implementiert werden.

### Speicherverwaltung

In Rust wird statt mit Raw-Pointern hauptsächlich mit Referenzen gearbeitet. Diese zeigen immer auf gültigen Speicher und dürfen somit niemals den Wert `null` annehmen. Standardmäßig sind in Rust alle Variablen unveränderbar, für veränderbare Variablen müssen diese mit dem Schlüsselwort `mut` definiert werden. 

Genauso gibt es auch bei Referenzen gemeinsame Referenzen (_shared references_), eingeleitet durch `&`, und veränderbare Referenzen (_mutable references_), eingeleitet durch `&mut`. Gemeinsame Referenzen dürfen wie der Name schon andeutet beliebig viele existieren. Veränderbare Referenzen dürfen nur einmal existieren. Referenzen dürfen nicht länger "leben", als das Objekt welches sie referenzieren. Damit garantiert der Rust-Compiler die Validität von Referenzen. 

Das Erstellen von Referenzen wird als _Borrowing_ bezeichnet. Außerdem hat jedes Objekt im Speicher einen Besitzer (_Ownership_). Diese beiden Konzepte bilden die Grundlage für eine Speicherverwaltung ohne Garbage Collection. Dynamische Speicherverwaltung ist mit Smart-Pointern wie `Box`, `Rc` und `RefCell` möglich. Dabei wird das System des Ownership und Borrowing einfach auf die Laufzeit übertragen.

Das Konzept von `null` ist in Rust nicht vorhanden. Die Abwesenheit eines Werts kann stattdessem mit dem Typ `Option<T>` modelliert werden, der tatsächlich nur ein einfaches `enum` mit den Möglichkeiten `Some(T)` und `None` ist.

### Fehlerbehandlung

In Rust wird zwischen behebbaren und nicht behebbaren Fehlern unterschieden. Bei nicht behebbaren Fehlern wird der Thread auf dem der Fehler auftritt aufgeräumt und beendet. Anders als bei anderen Sprachen gibt es in Rust keine Exceptions. Stattdessen können behebbare Fehler durch ganz normale Rückgabewerte modelliert, wie das schon oben beschriebene `Option<T>` oder das etwas ausdrucksstärkere `Result<T,E>`. Genauso wie `Option` ist auch `Result` einfach nur ein `enum` mit den Möglichkeiten `Ok(T)` und `Err(E)`.

## Toolchain

TODO