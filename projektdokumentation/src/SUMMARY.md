# Zusammenfassung

- [Einführung](./einfuehrung.md)

# Bedienungsanleitung
- [Ausführen der Anwendung](./bedienung/ausfuehren.md)
- [Allgemeine Bedienung](./bedienung/allgemeine_bedienung.md)
- [Bekannte Probleme](./bedienung/probleme.md)

# Verwendete Technologie
- [Rust](./technologie/rust.md)
    [//]: # (Sprachdesign)
    [//]: # (Toolchain)
    [//]: # (Dependencies mit Cargo.toml)
- [vCard](./technologie/vcard.md)
    [//]: # (Spezifikation)
    [//]: # (vobject)
- [WebAssembly](./technologie/webassembly.md)
    [//]: # (Was ist Webassembly?)
    [//]: # (WebAssembly mit Rust)
        [//]: # (Tools: wasm-pack, etc)
        [//]: # (Einbinden in Webseite)
        [//]: # (Libraries: wasm-bindgen, js-sys, web-sys, console_error_panic_hook, wee_alloc)
    [//]: # (yew)
- [PDF](./technologie/pdf.md)
    [//]: # (Wie funktioniert PDF?)
    [//]: # (genpdf)
- [QR Code](./technologie/qrcode.md)
    [//]: # (Wie funktionieren QR Codes?)
    [//]: # (qrcodegen)
- [Weitere Technologien](./technologie/weitere.md) 
    [//]: # (base64, chrono, uuid, boolinator)

# Eigenleistung
- [Eigenleistung](./eigenleistung.md)

# Schlusswort
- [Fazit](./fazit.md)
- [Quellen](./quellen.md)