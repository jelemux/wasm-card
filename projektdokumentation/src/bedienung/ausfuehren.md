# Ausführen der Anwendung

## Tools installieren

Benötigt werden:
- die <a href="https://www.git-scm.com/" target="_blank">Source Code Verwaltung Git</a>
- die <a href="https://www.rust-lang.org/" target="_blank">Rust</a> Toolchain
- <a href="https://github.com/rustwasm/wasm-pack" target="_blank">`wasm-pack`</a>
- <a href="https://github.com/sagiegurari/cargo-make" target="_blank">`cargo-make`</a>

Anweisungen zur Installation von Git finden sich <a href="https://www.git-scm.com/book/de/v2/Erste-Schritte-Git-installieren" target="_blank">hier</a>.

Die Rust Toolchain wird üblicherweise durch <a href="https://github.com/rust-lang/rustup" target="_blank">`rustup`</a> installiert.  
Die Installationsanweisung findet sich auf <a href="https://www.rust-lang.org/tools/install" target="_blank">dieser Webseite</a>.

`wasm-pack` kann entweder mit dem Installer auf <a href="https://rustwasm.github.io/wasm-pack/installer/" target="_blank">dieser Webseite</a> oder  
mit dem Befehl `cargo install wasm-pack` installiert werden.

`cargo-make` kann mit dem Befehl `cargo install cargo-make` installiert werden.

## Repository klonen

```
git clone https://codeberg.org/jelemux/wasm-card.git
```

## Kompilieren der Anwendung

Im Debug Modus:
```
cargo make build
```
Im Release Modus (mehr Optimierungen, dauert länger):
```
cargo make build_release
```

## Bereitstellen der Anwendung

```
cargo make serve
```
Die Anwendung ist dann unter <a href="http://0.0.0.0:8000" target="_blank">http://0.0.0.0:8000</a> abrufbar.  
Mit dem Argument `--port` lässt sich der Port umstellen.