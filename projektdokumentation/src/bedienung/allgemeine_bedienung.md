# Allgemeine Bedienung

## Eigenschaften eingeben

Kontaktdaten können einfach in die entsprechenden Eingabefelder eingegeben werden.

<img src="./screenshot-eigenschaften_name.png"/>  
In diese Gruppe von Eingabefeldern können beispielsweise die verschiedenen Teile des Namens der Person eigegeben werden.

## Hinzufügen und Entfernen von Gruppen

Es können beliebig viele Instanzen einer Gruppe hinzugefügt werden. Dafür einfach den _Add more_ Button neben der Gruppenüberschrift klcken.

Um die Instanz einer Gruppe wieder zu entfernen, kann das X rechts oben in dieser geklickt werden.

<img src="./screenshot-gruppe_hinzufuegen.png"/>  
Hier wurde zum Beispiel eine zweite Gruppe für den Namen hinzugefügt.

## Generieren von vCard, PDF und QR Code

Neben den _Generate_ Button lässt sich auswählen, ob eine VCard, ein PDF oder ein QR Code generiert werden soll. 

<img src="./screenshot-generieren_auswahl.png"/>

Wird dann auf _Generate_ geklickt, wird die entsprechende Datei generiert und unten in der Vorschau angezeigt. Außerdem erscheint ein _Download_ Button, über den die Datei gespeichert werden kann.

<img src="./screenshot-generieren_download.png"/>