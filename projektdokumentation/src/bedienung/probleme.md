# Probleme

## Entfernen aller Instanzen von allen Gruppen

Wenn alle Instanzen von allen Gruppen entfernt werden, dann hängt sich die Anwendung auf und es kommt eine Meldung dass die Webseite den Browser verlangsamt mit der Option, sie anzuhalten.

Ich nehme an dass dieses Problem etwas mit den Smart-Pointern bzw. `WeakComponentLinks`in meinem Programm zu tun hat.

## Entfernen der Gruppen in der richtigen Reihenfolge

Obwohl beim Entfernen der Gruppen diese immer mit dem richtigen Index entfernt werden, wird in der Oberfläche die letzte Gruppe entfernt und nicht die, bei welcher man auf das X klickt.

Hier nehme ich an, dass der Browser (oder yew) nicht versteht, dass eine Gruppe aus der Mitte gelöscht wurde und dann der eingegebene Text zwischen den Gruppen verschoben wird.

## Größe der Binary

Aktuell ist die kompilierte Binary sehr groß und würde daher entsprechend lange brauchen, um vom Server an den Browser übertragen zu werden. 

Dies liegt vor allem daran, dass die Font Dateien zum Erzeugen des PDFs mit in die Binary kompiliert werden.
Die Font Dateien werden von der `genpdf-rs` Bibliothek eigentlich nur dazu benötigt um die nötigen Metriken auszulesen. Ich habe schon nach einer Lösung gesucht, die Font-Metriken direkt mitzugeben, bisher hat das allerdings noch nicht funktioniert.

## Vorschau des QR Codes verschoben

Die Vorschau des QR Codes der generiert wird ist leider etwas nach unten links verschoben.  
Dies sollte sich durch geschickte Anpassungen im HTML oder CSS beheben lassen.