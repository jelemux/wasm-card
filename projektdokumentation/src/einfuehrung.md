# Einführung

Dies ist die Projektdokumentation zum Hausmesseprojekt `wasm-card` von Jeremias Weber.

## Zweck der Software

Der Zweck dieser Software besteht darin, Benutzern eine einfache Möglichkeit zu bieten digitale Visitenkarten, sogenannte **[VCards](./technologie/vcard.md)** online zu erstellen und herunterzuladen. Die Kontaktdaten können zudem auch als **[QR Code](./technologie/qrcode.md)** im SVG-Format und als **[PDF](./technologie/pdf.md) mit druckfertigen Visitenkarten** angezeigt und heruntergeladen werden.

Dazu bietet diese Software ein einfaches und flexibles Formular, in welches die Kontaktdaten eingetragen werden können.

Obwohl diese Software als Webseite verfügbar gestellt wird können um ihre Privatsphäre besorgte Nutzer sind beruhigt sein, da ihre Daten an keinen Server übermittelt werden und allein in ihrem Browser bleiben. Dies ist möglich, da die Anwendung bloß eine **statische Webseite** ist und die Verarbeitung der Daten mittels **[WebAssembly](./technologie/webassembly.md)** vollständig im Browser abläuft.

## Zielgruppe

Diese Software wendet sich an alle Benutzer, die nach einer Möglichkeit suchen einfach und sicher digitale sowie analoge Visitenkarten online zu erstellen um somit ihre Kontakdaten zu teilen.