<div align="center">

  <h1><code>wasmCard</code></h1>
  <strong>An Online vCard Generator written in Rust.</strong>

</div>

## 📚 About

A WebAssebmly VCard Generator sporting the [yew frontend framework](https://yew.rs).

Supports generating vCards (.vcf), print-ready PDF business cards and QR Codes.

There is no server side code, everything is executed client-side.

## 🎮 Features

* Supported properties as of yet: name, address, telephone, birthday, anniversary
* Output and Download as vCard (.vcf)
* Output and Download as QR Code
* Generates a dummy PDF

## 🗺 Roadmap

* Add more properties
* Put generation and download both in download button
* Generate PDF with print-ready business cards
* Probably a lot of refactoring - this thing is a mess!

## 🚲 Usage

### 🛠️ Needed tools

* [Standard rust toolchain](https://www.rust-lang.org/tools/install)
* [wasm-pack](https://rustwasm.github.io/wasm-pack/installer/)
* cargo-make: `cargo install cargo-make`


### 📦 Build
Debug:
```
cargo make build
```
Release:
```
cargo make build_release
```

### 🍲 Serve

```
cargo make serve
```

### 🔍 Linting

```
cargo make clippy
```

### 📏 Format
```
cargo make fmt
```